@echo off

:: Set up cmd
regedit /s cmdstartup.reg
regedit /s cmdcodedark.reg

:: Install alias scripts and an init script
mkdir %userprofile%\.cmdalias
copy cmdalias\* %userprofile%\.cmdalias\
copy cmdrc.bat %userprofile%\.cmdrc.bat

:: Set up Windows Terminal, if installed
set windows_terminal_settings=%userprofile%\AppData\Local\Packages\Microsoft.WindowsTerminal*\LocalState\
if exist %windows_terminal_settings% (
	del %windows_terminal_settings%\settings.json
	copy settings.json %windows_terminal_settings%\
)
