@echo off
:: Encoding of this file is cp852, it has to be in order to corectly display
:: table drawing glyphs present in the prompt.

:: Codepage for displaing UTF-8
	rem chcp 65001 

:: Default Codepage - OEM 852
	rem chcp 852

:: Typing %userprofile% is too tedious
	set up=%userprofile%

:: Make dir look like ls, but without colors
	set DIRCMD=/d

:: Path
	set PATH=%PATH%;%userprofile%\.cmdalias

:: Custom Prompts
	rem prompt %username%@%computername%:$p$$$s
	rem prompt ��[%username%@%computername%:$p]$_��[$+]$$$s
	rem prompt �[%username%@%computername%:$p]$_�[$+]$$$s
	rem prompt ��[[32m%username%[37m@[32m%computername%[37m:[35m$p[37m]$_�[$+]$$$s
	prompt ��[[32m%username%[0m@[32m%computername%[0m:[35m$M$p[0m]$_�[$+]$$$s
	rem prompt ��[%username%@%computername%:$M$p]$_�[$+]$$$s
